﻿using Exam.CodeFirst.DataModels;
using UIExam.ViewModels;
using AutoMapper;

namespace UIExam.Mapper;

public class Mapping
{
    public void Create()
    {
        AutoMapper.Mapper.CreateMap<DataModel, DataViewModel>();
        AutoMapper.Mapper.CreateMap<DataViewModel, DataModel>();

        AutoMapper.Mapper.CreateMap<Dish, DishViewModel>();
        AutoMapper.Mapper.CreateMap<DishViewModel, Dish>();
    }
}