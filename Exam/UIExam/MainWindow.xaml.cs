﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exam.CodeFirst.DataModels;

namespace UIExam
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Context ctx = new Context();
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Savebtn_OnClick(object sender, RoutedEventArgs e)
        {
            string name = txtDishName.Text;
            double price = Double.Parse(txtDishPrice.Text);
            double weight = Double.Parse(txtDishWeight.Text);
            double energyValue = Double.Parse(txtDishEnergyValue.Text);

            ctx.Dishes.Add(new Dish() {Name = name, Price = price, Weight = weight, EnergyValue = energyValue});
            ctx.SaveChanges();
            
            txtDishName.Clear();
            txtDishPrice.Clear();
            txtDishWeight.Clear();
            txtDishEnergyValue.Clear();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var currentRowIndex = DataGridDishes.SelectedIndex;
            Console.WriteLine(currentRowIndex);
            Console.WriteLine(ctx.Dishes.ToList()[currentRowIndex]);
            // List<Dish> dishesToDelete = ctx.Dishes.Where(x => x.DishId == currentRowIndex).ToList();
            Dish dishToDelete = ctx.Dishes.ToList()[currentRowIndex];
            ctx.Remove(dishToDelete);
            ctx.SaveChanges();
        }
    }
}