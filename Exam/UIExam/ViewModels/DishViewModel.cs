﻿namespace UIExam.ViewModels;

public class DishViewModel : ViewModelBase
{
    private string _name;
    public string Name {
        get { return _name; }
        set
        {
            _name = value;
            OnPropertyChanged("Name");
        }
    }
    
    private string _weight;
    public string Weight {
        get { return _weight; }
        set
        {
            _weight = value;
            OnPropertyChanged("Weight");
        }
    }
    
    private string _energyValue;
    public string EnergyValue {
        get { return _energyValue; }
        set
        {
            _energyValue = value;
            OnPropertyChanged("EnergyValue");
        }
    }
    
    private string _price;
    public string Price {
        get { return _price; }
        set
        {
            _price = value;
            OnPropertyChanged("Price");
        }
    }
}