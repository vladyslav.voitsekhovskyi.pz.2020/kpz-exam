﻿using System.Collections.ObjectModel;
using Microsoft.EntityFrameworkCore;

namespace UIExam.ViewModels;

public class DataViewModel : ViewModelBase
{
    private ObservableCollection<DishViewModel> _dishes;
        public ObservableCollection<DishViewModel> Dishes
        {
            get { return _dishes; }
            set
            {
                _dishes = value;
                OnPropertyChanged("Dishes");
            }
        }
}