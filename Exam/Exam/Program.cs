﻿// See https://aka.ms/new-console-template for more information

using Exam.CodeFirst.DataModels;
using Microsoft.EntityFrameworkCore;


//CodeFirst
Context context = new Context();


//Inserting data
// context.Dishes.Add(new Dish()
// {
//     Name = "Картопляне пюре",
//     Weight = 200,
//     EnergyValue = 600,
//     Price = 20.50
// });
//
//
// context.SaveChanges();



//Updating data
// var dish = context.Dishes.FirstOrDefault(c => c.Name.StartsWith("O"));
// dish.Name = "Картопляне пюре";
//
// context.SaveChanges(); 


//Selecting data
Console.WriteLine("\nDishes:");
context.Dishes.ToList().ForEach(o => Console.WriteLine(o));



