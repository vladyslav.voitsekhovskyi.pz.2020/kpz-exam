﻿namespace Exam.CodeFirst.DataModels;

public class DataModel
{
        Context ctx = new Context();
        public DataModel()
        {
            Dishes = ctx.Dishes.ToList();
        }
        public IEnumerable<Dish> Dishes { get; set; }


        public static DataModel Load()
        {
            return new DataModel();
        }
        
        public void Update()
        {
            List<Dish> oldDishes = ctx.Dishes.ToList();

            if (oldDishes.Count() == Dishes.Count())
            {
                for (int i = 0; i < oldDishes.Count; i++)
                {
                    if (oldDishes[i] != Dishes.ToList()[i])
                    {
                        List<Dish> dishToUpdate = ctx.Dishes.Where(x => x.DishId == oldDishes[i].DishId).ToList();
                        dishToUpdate[0].Name = Dishes.ToList()[i].Name;
                        dishToUpdate[0].Weight = Dishes.ToList()[i].Weight;
                        dishToUpdate[0].Price = Dishes.ToList()[i].Price;
                        dishToUpdate[0].EnergyValue = Dishes.ToList()[i].EnergyValue;
                        ctx.SaveChanges();
                    }
                }
            }
        }
}