﻿using System.ComponentModel.DataAnnotations;

namespace Exam.CodeFirst.DataModels;

public class Dish
{
    [Key]
    public int DishId { get; set; }

    [MaxLength(20),MinLength(2)]
    public String Name { get; set; }
    
    public Double Weight { get; set; }

    public Double EnergyValue { get; set; }
    
    public Double Price { get; set; }

    public override string ToString()
    {
        return $"{nameof(DishId)}: {DishId}, {nameof(Name)}: {Name}, {nameof(Weight)}: {Weight}, {nameof(EnergyValue)}: {EnergyValue}, {nameof(Price)}: {Price}";
    }
}

