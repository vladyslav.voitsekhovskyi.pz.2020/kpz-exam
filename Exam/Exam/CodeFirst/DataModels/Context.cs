﻿using Microsoft.EntityFrameworkCore;

namespace Exam.CodeFirst.DataModels;

public class Context : DbContext
{

    public Context(DbContextOptions<Context> options) : base(options)
    {
        
    }
    
    public Context()
    {
        
    }
    
    public DbSet<Dish> Dishes { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer("Integrated Security=SSPI;Initial Catalog=ExamKPZ;Data Source=DESKTOP-CB9DGS0;");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Dish>().HasData(
            new Dish(){DishId = 1, Name = "Борщ", Weight = 300, EnergyValue = 580, Price = 35},
            new Dish(){DishId = 2, Name = "Котлета", Weight = 120, EnergyValue = 330, Price = 28}
        );
    }
}