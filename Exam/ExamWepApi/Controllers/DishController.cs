﻿using Exam.CodeFirst.DataModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ExamWepApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class DishController : ControllerBase
{
    private Context _context;

    public DishController(Context context) => _context = context;

    [HttpGet]
    public async Task<IEnumerable<Dish>> Get()
    {
        return await _context.Dishes.ToListAsync();
    }
    
    [HttpGet("{id}")]
    public async Task<IActionResult> GetById(int id)
    {
        var dish = await _context.Dishes.FindAsync(id);
        return dish == null ? NotFound() : Ok(dish);
    }
    
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<IActionResult> CreateDish(Dish dish)
    {
        _context.Dishes.Add(dish);
        await _context.SaveChangesAsync();
    
        return CreatedAtAction(nameof(GetById), new {id = dish.DishId}, dish);
    }
    
    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> UpdateDish(int id, Dish dish)
    {
        if (id != dish.DishId) return BadRequest();
    
        _context.Entry(dish).State = EntityState.Modified;
        await _context.SaveChangesAsync();
    
        return NoContent();
    }
    
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> DeleteDish(int id)
    {
        var dish = _context.Dishes.Find(id);
        if (dish == null) return NoContent();
    
        _context.Dishes.Remove(dish);
        await _context.SaveChangesAsync();
    
        return NoContent();
    }
}